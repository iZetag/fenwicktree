//
//  FenwickTree.hpp
//  FenwickTree
//
//  Created by Fedor Nesterenko on 05/06/14.
//
//

#ifndef FenwickTree_FenwickTree_hpp
#define FenwickTree_FenwickTree_hpp

#include <vector>
#include <cassert>

template<typename T>
class FenwickTree
{
public:
    FenwickTree(std::size_t n)
        : f_(n, 0)
    {
    }
    T get(int i) const;
    T operator[](std::size_t i) const;
    void set(int i, T val);
    void update(int i, T delta);
//    void push(T val);
    T sum(int left, int right) const;
    T sum(int right) const;
private:
    std::vector<T> f_;
};

//template<typename T>
//inline void FenwickTree<T>::push(T val)
//{
//    f_.push_back(val);
//}

template<typename T>
inline T FenwickTree<T>::get(int i) const
{
    assert(i < f_.size());
    T sum = f_[i];
    if( i > 0 )
    {
        int z = (i & (i + 1)) - 1;
        --i;
        while(i != z)
        {
            sum -= f_[i];
            i = (i & (i + 1)) - 1;
        }
    }
    return sum;
}
template<typename T>
inline void FenwickTree<T>::set(int i, T val)
{
    assert(i < f_.size());
    const auto delta = val - get(i);
    update(i, delta);
}

template<typename T>
inline void FenwickTree<T>::update(int i, T delta)
{
    const auto n = f_.size();
    while(i < n)
    {
        f_[i] += delta;
        i |= i + 1;
    }
}
template<typename T>
inline T FenwickTree<T>::sum(int right) const
{
    //assert(right < f_.size());
    auto sum = T{};
    while(right >= 0)
    {
        sum += f_[right];
        right = (right & (right + 1)) - 1;
    }
    return sum;
}
template<typename T>
inline T FenwickTree<T>::sum(int left, int right) const
{
    return sum(right) - sum(left-1);
}

template<typename T>
inline T FenwickTree<T>::operator[](std::size_t i) const
{
    assert(i < f_.size());
    
}

#endif
